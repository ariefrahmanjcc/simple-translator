# simpleTranslator

## Description

Translate from English to Indonesia

Source: english
target: indonesia

## how to deploy
install the apk file on your Android device. 

## Demo Video Link
https://www.youtube.com/watch?v=FTHtIxZ7oEY

## Package
- translate-google-api 1.0.4
- axios

## info
the source.txt and target.txt is in this repository.

