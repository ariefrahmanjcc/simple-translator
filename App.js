import React, { useState } from "react";
import { StyleSheet, Text, View, TextInput, Button } from "react-native";
import translate from "translate-google-api";

export default function App() {
  const [kata, setKata] = useState("");
  const [hasil, setHasil] = useState("");

  const submit = async () => {
    const result = await translate(kata, {
      tld: "com",
      to: "id",
    });
    setHasil(result);
  };
  return (
    <View style={styles.container}>
      <Text style={{ alignSelf: "center", marginBottom: 10 }}>
        Translate English to Indonesia
      </Text>
      <TextInput
        value={kata}
        style={{
          fontSize: 24,
          color: "steelblue",
          borderWidth: 1,
          borderRadius: 10,
          margin: 10,
          padding: 10,
        }}
        placeholder="Type here..."
        onChangeText={(kata) => {
          setKata(kata);
        }}
      />
      <Button title="translate" onPress={submit} />
      <Text
        style={{
          fontSize: 24,
          color: "steelblue",
          borderWidth: 1,
          borderRadius: 10,
          margin: 10,
          padding: 10,
        }}
      >
        {hasil}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
    padding: 10,
  },
});
